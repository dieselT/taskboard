﻿namespace Tests.PagingService
{
    using NUnit.Framework;
    using Web.Definitions;
    using Web.Models;

    public abstract class PagesBaseTest
    {
        protected void AssertPageElement(PagingElement page, PagingElementType expectedType, bool expectedIsCurrentPage, int expectedPageNumber)
        {
            Assert.AreEqual(expectedType, page.Type);
            Assert.AreEqual(expectedIsCurrentPage, page.IsCurrentPage);
            Assert.AreEqual(expectedPageNumber, page.PageNumber);
        }
    }
}