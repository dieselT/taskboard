﻿namespace Tests.PagingService
{
    using NUnit.Framework;
    using Stubs;
    using Web.Definitions;
    using Web.Services.Impl;

    [TestFixture]
    public class MaketTests : PagesBaseTest
    {
        private readonly PagingService _pagingService;
        private const int PageSize = 10;
        private const int PagesCount = 193;
        private const int TotalCount = PagesCount * PageSize;

        public MaketTests()
        {
            _pagingService = new PagingService(new StubUrlHelper());
        }

        [Test]
        public void FirstPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 1);

            Assert.AreEqual(8, pages.Length);

            AssertPageElement(pages[0], PagingElementType.CertainPage, true, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 5);
            Assert.AreEqual(PagingElementType.Separator, pages[5].Type);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[7], PagingElementType.NextPage, false, 2);
        }

        [Test]
        public void ThirdPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 3);

            Assert.AreEqual(9, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 2);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, true, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 5);
            Assert.AreEqual(PagingElementType.Separator, pages[6].Type);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[8], PagingElementType.NextPage, false, 4);
        }

        [Test]
        public void FifthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 5);

            Assert.AreEqual(11, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 4);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 5);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 6);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 7);
            Assert.AreEqual(PagingElementType.Separator, pages[8].Type);
            AssertPageElement(pages[9], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[10], PagingElementType.NextPage, false, 6);
        }

        [Test]
        public void SixthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 6);

            Assert.AreEqual(11, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 5);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            Assert.AreEqual(PagingElementType.Separator, pages[2].Type);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 6);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 7);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 8);
            Assert.AreEqual(PagingElementType.Separator, pages[8].Type);
            AssertPageElement(pages[9], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[10], PagingElementType.NextPage, false, 7);
        }

        [Test]
        public void _188PageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 188);

            Assert.AreEqual(11, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 187);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            Assert.AreEqual(PagingElementType.Separator, pages[2].Type);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 186);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 187);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 188);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 189);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 190);
            Assert.AreEqual(PagingElementType.Separator, pages[8].Type);
            AssertPageElement(pages[9], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[10], PagingElementType.NextPage, false, 189);
        }

        [Test]
        public void _189PageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 189);

            Assert.AreEqual(11, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 188);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            Assert.AreEqual(PagingElementType.Separator, pages[2].Type);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 187);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 188);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 189);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 190);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 191);
            AssertPageElement(pages[8], PagingElementType.CertainPage, false, 192);
            AssertPageElement(pages[9], PagingElementType.CertainPage, false, 193);
            AssertPageElement(pages[10], PagingElementType.NextPage, false, 190);
        }

        [Test]
        public void LastPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, PagesCount);

            Assert.AreEqual(8, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 192);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            Assert.AreEqual(PagingElementType.Separator, pages[2].Type);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 189);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 190);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 191);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 192);
            AssertPageElement(pages[7], PagingElementType.CertainPage, true, 193);
        }
    }
}