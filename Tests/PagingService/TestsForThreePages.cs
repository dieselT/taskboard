﻿namespace Tests.PagingService
{
    using NUnit.Framework;
    using Stubs;
    using Web.Definitions;
    using Web.Services.Impl;

    [TestFixture]
    public class TestsForThreePages : PagesBaseTest
    {
        private const int PageSize = 10;
        private const int PagesCount = 3;
        private const int TotalCount = PagesCount*PageSize;
        private readonly PagingService _pagingService;

        public TestsForThreePages()
        {
            _pagingService = new PagingService(new StubUrlHelper());
        }

        [Test]
        public void FirstPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 1);

            Assert.AreEqual(4, pages.Length);

            AssertPageElement(pages[0], PagingElementType.CertainPage, true, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[3], PagingElementType.NextPage, false, 2);
        }

        [Test]
        public void SecondPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 2);

            Assert.AreEqual(5, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, true, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.NextPage, false, 3);
        }

        [Test]
        public void ThirdPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 3);

            Assert.AreEqual(4, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 2);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, true, 3);
        }
    }
}