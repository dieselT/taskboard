﻿namespace Tests.PagingService
{
    using NUnit.Framework;
    using Stubs;
    using Web.Definitions;
    using Web.Services.Impl;

    [TestFixture]
    public class TestsForFivePages : PagesBaseTest
    {
        private const int PageSize = 10;
        private const int PagesCount = 5;
        private const int TotalCount = PagesCount*PageSize;
        private readonly PagingService _pagingService;

        public TestsForFivePages()
        {
            _pagingService = new PagingService(new StubUrlHelper());
        }

        [Test]
        public void FirstPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 1);

            Assert.AreEqual(6, pages.Length);

            AssertPageElement(pages[0], PagingElementType.CertainPage, true, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[5], PagingElementType.NextPage, false, 2);
        }

        [Test]
        public void SecondPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 2);

            Assert.AreEqual(7, pages.Length);
            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, true, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[6], PagingElementType.NextPage, false, 3);
        }

        [Test]
        public void ThirdPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 3);

            Assert.AreEqual(7, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 2);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, true, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[6], PagingElementType.NextPage, false, 4);
        }

        [Test]
        public void FourthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 4);

            Assert.AreEqual(7, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 3);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, true, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[6], PagingElementType.NextPage, false, 5);
        }

        [Test]
        public void FivthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 5);

            Assert.AreEqual(6, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 4);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 5);
        }
    }
}