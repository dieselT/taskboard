﻿namespace Tests.PagingService
{
    using NUnit.Framework;
    using Stubs;
    using Web.Definitions;
    using Web.Services.Impl;

    [TestFixture]
    public class TestsForNinePages : PagesBaseTest
    {
        private readonly PagingService _pagingService;
        private const int PageSize = 10;
        private const int PagesCount = 9;
        private const int TotalCount = PagesCount * PageSize;

        public TestsForNinePages()
        {
            _pagingService = new PagingService(new StubUrlHelper());
        }

        [Test]
        public void FirstPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 1);

            Assert.AreEqual(8, pages.Length);

            AssertPageElement(pages[0], PagingElementType.CertainPage, true, 1);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 5);
            Assert.AreEqual(PagingElementType.Separator, pages[5].Type);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 9);
            AssertPageElement(pages[7], PagingElementType.NextPage, false, 2);
        }

        [Test]
        public void NinthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 9);

            Assert.AreEqual(8, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 8);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            Assert.AreEqual(PagingElementType.Separator, pages[2].Type);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 5);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 6);
            AssertPageElement(pages[5], PagingElementType.CertainPage, false, 7);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 8);
            AssertPageElement(pages[7], PagingElementType.CertainPage, true, 9);
        }

        [Test]
        public void FifthPageAsCurrentTest()
        {
            var pages = _pagingService.CreatePagings(PageSize, TotalCount, 5);

            Assert.AreEqual(11, pages.Length);

            AssertPageElement(pages[0], PagingElementType.PreviousPage, false, 4);
            AssertPageElement(pages[1], PagingElementType.CertainPage, false, 1);
            AssertPageElement(pages[2], PagingElementType.CertainPage, false, 2);
            AssertPageElement(pages[3], PagingElementType.CertainPage, false, 3);
            AssertPageElement(pages[4], PagingElementType.CertainPage, false, 4);
            AssertPageElement(pages[5], PagingElementType.CertainPage, true, 5);
            AssertPageElement(pages[6], PagingElementType.CertainPage, false, 6);
            AssertPageElement(pages[7], PagingElementType.CertainPage, false, 7);
            AssertPageElement(pages[8], PagingElementType.CertainPage, false, 8);
            AssertPageElement(pages[9], PagingElementType.CertainPage, false, 9);
            AssertPageElement(pages[10], PagingElementType.NextPage, false, 6);
        }
    }
}