﻿namespace Tests.Domain
{
    using System;
    using global::Domain;
    using global::Domain.Definitions;
    using global::Domain.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class TaskStateTests
    {
        private readonly User _user = new User("test", new Password("test"));

        [Test]
        public void DoneTaskAlwaysDoneRegardlessOfExecutionDate()
        {
            var todaySheduledTask = new Task("Test", DateTime.Today, _user, true);
            var overdueTask = new Task("Test", DateTime.Today.AddDays(-2), _user, true);
            var futureScheduledTask = new Task("Test", DateTime.Today.AddDays(2), _user, true);

            Assert.AreEqual(TaskState.Done, todaySheduledTask.State);
            Assert.AreEqual(TaskState.Done, overdueTask.State);
            Assert.AreEqual(TaskState.Done, futureScheduledTask.State);
        }


        [Test]
        public void OverdueAndNotDoneTaskHasOverdueState()
        {
            var task = new Task("Test", DateTime.Today.AddDays(-2), _user);

            Assert.AreEqual(TaskState.Overdue, task.State);
        }

        [Test]
        public void TodaySheduledAndNotDoneTaskHasTodaySheduledState()
        {
            var task = new Task("Test", DateTime.Today, _user);

            Assert.AreEqual(TaskState.TodayScheduled, task.State);
        }

        [Test]
        public void FutureScheduledAndNotDoneTaskHasFutureScheduledState()
        {
            var task = new Task("Test", DateTime.Today.AddDays(2), _user);

            Assert.AreEqual(TaskState.FutureScheduled, task.State);
        }
    }
}