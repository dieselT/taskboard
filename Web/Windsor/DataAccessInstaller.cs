﻿namespace Web.Windsor
{
    using Castle.Facilities.TypedFactory;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using DataAccess;

    public class DataAccessInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<TypedFactoryFacility>();

            container.Register(Component.For<IUnitOfWorkFactory>().AsFactory().LifestyleSingleton(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient(),
                Component.For<TaskBoardContext>().ImplementedBy<TaskBoardContext>().LifestyleTransient());
        }
    }
}