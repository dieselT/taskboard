﻿namespace Web.Windsor
{
    using Authentication;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Services;
    using Services.Impl;

    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<BasicAuthenticationAttribute>().ImplementedBy<BasicAuthenticationAttribute>(),
                Component.For<IAuthenticationService>().ImplementedBy<AuthenticationService>(),
                Component.For<IPagingUrlHelper>().ImplementedBy<PagingUrlHelper>(),
                Component.For<IPagingService>().ImplementedBy<PagingService>());
        }
    }
}