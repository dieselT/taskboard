﻿namespace Web.Models
{
    using System;
    using Definitions;

    public class PagingElement
    {
        public PagingElement(PagingElementType type, int? pageNumber = null, string url = null, int? currentPage = null)
        {
            Type = type;
            Url = url;

            if (pageNumber.HasValue)
            {
                if (currentPage.HasValue == false)
                    throw new ArgumentNullException(nameof(currentPage), $"Current page can't be null if pass {nameof(pageNumber)}");

                PageNumber = pageNumber.Value;
                IsCurrentPage = Type == PagingElementType.CertainPage &&
                                currentPage.Value == pageNumber.Value;
            }
        }

        public PagingElementType Type { get; set; }
        public string Url { get; set; }
        public int? PageNumber { get; set; }
        public bool IsCurrentPage { get; set; }
    }
}