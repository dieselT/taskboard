﻿namespace Web.Definitions
{
    public enum PagingElementType
    {
        PreviousPage,
        CertainPage,
        Separator,
        NextPage
    }
}