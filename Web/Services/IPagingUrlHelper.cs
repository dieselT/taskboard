﻿namespace Web.Services
{
    public interface IPagingUrlHelper
    {
        string CreateUrl(int page);
    }
}