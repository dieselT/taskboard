﻿namespace Web.Services
{
    using Models;

    public interface IPagingService
    {
        PagingElement[] CreatePagings(int pageSize, int totalCount, int currentPage);
    }
}