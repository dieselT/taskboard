﻿namespace Web.Services
{
    public interface IAuthenticationService
    {
        void SignIn(int userId);
        int GetCurrentUserId();
        bool IsAuthenticated { get; }
    }
}