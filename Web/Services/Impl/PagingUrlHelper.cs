namespace Web.Services.Impl
{
    using System.Web;
    using System.Web.Mvc;

    public class PagingUrlHelper : IPagingUrlHelper
    {
        private const int FirstPage = 1;

        public string CreateUrl(int page)
        {
            return page == FirstPage
                ? new UrlHelper(HttpContext.Current.Request.RequestContext).Action("Index", "Home")
                : new UrlHelper(HttpContext.Current.Request.RequestContext).Action("Index", "Home", new { page });
        }
    }
}