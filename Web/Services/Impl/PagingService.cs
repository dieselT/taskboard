namespace Web.Services.Impl
{
    using System;
    using System.Collections.Generic;
    using Definitions;
    using Models;

    public class PagingService : IPagingService
    {
        private readonly IPagingUrlHelper _urlHelper;
        private const int FirstPage = 1;

        public PagingService(IPagingUrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        public PagingElement[] CreatePagings(int pageSize, int totalCount, int currentPage)
        {
            var result = new List<PagingElement>();

            var page = currentPage;
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

            if (currentPage <= 0)
                page = FirstPage;
            else if (currentPage > totalPages)
                page = totalPages;

            if (page > FirstPage)
            {
                var pageNumber = page - 1;
                result.Add(new PagingElement(PagingElementType.PreviousPage, pageNumber,
                    _urlHelper.CreateUrl(pageNumber), pageNumber));
            }

            var minPageRange = page - 2;
            var maxPageRange = page + 2;
            var minRangeDecrease = 0;
            var maxRangeIncrease = 0;

            if (minPageRange <= 0)
            {
                maxRangeIncrease = FirstPage - minPageRange;
                minPageRange = FirstPage;
            }

            if (maxPageRange > totalPages)
            {
                minRangeDecrease = maxPageRange - totalPages;
                maxPageRange = totalPages;
            }

            var decreasedMinPageRange = minPageRange - minRangeDecrease;
            if (decreasedMinPageRange > 1)
                minPageRange = decreasedMinPageRange;

            var increasedMaxPageRange = maxPageRange + maxRangeIncrease;
            if (increasedMaxPageRange < totalPages)
                maxPageRange = increasedMaxPageRange;

            if (minPageRange > 1)
            {
                result.Add(new PagingElement(PagingElementType.CertainPage, FirstPage, _urlHelper.CreateUrl(FirstPage),
                    currentPage));

                if (minPageRange == FirstPage + 2)
                {
                    var pageNumber = FirstPage + 1;
                    result.Add(new PagingElement(PagingElementType.CertainPage, pageNumber,
                        _urlHelper.CreateUrl(pageNumber),
                        currentPage));
                }
                else if (minPageRange != FirstPage +1)
                    result.Add(new PagingElement(PagingElementType.Separator));
            }

            for (var i = minPageRange; i <= maxPageRange; i++)
                result.Add(new PagingElement(PagingElementType.CertainPage, i, _urlHelper.CreateUrl(i), currentPage));

            if (maxPageRange < totalPages)
            {
                if (maxPageRange == totalPages - 2)
                {
                    var pageNumber = totalPages - 1;
                    result.Add(new PagingElement(PagingElementType.CertainPage, pageNumber,
                        _urlHelper.CreateUrl(pageNumber), currentPage));
                }
                else if (maxPageRange != totalPages - 1)
                    result.Add(new PagingElement(PagingElementType.Separator));

                result.Add(new PagingElement(PagingElementType.CertainPage, totalPages, _urlHelper.CreateUrl(totalPages),
                    currentPage));
            }

            if (page < totalPages)
            {
                var pageNumber = page + 1;
                result.Add(new PagingElement(PagingElementType.NextPage, pageNumber, _urlHelper.CreateUrl(pageNumber),
                    currentPage));
            }

            return result.ToArray();
        }
    }
}