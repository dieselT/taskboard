﻿namespace Web.Services.Impl
{
    using System;
    using System.Security.Principal;
    using System.Web;

    public class AuthenticationService : IAuthenticationService
    {
        public void SignIn(int userId)
        {
            var identity = new GenericIdentity(userId.ToString());
            var principal = new GenericPrincipal(identity, null);
            HttpContext.Current.User = principal;
        }

        public int GetCurrentUserId()
        {
            if (IsAuthenticated == false)
                throw new UnauthorizedAccessException();

            return int.Parse(HttpContext.Current.User.Identity.Name);
        }

        public bool IsAuthenticated => HttpContext.Current.User.Identity.IsAuthenticated;
    }
}