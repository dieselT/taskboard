﻿namespace Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using DataAccess;
    using Domain.Definitions;
    using Domain.Entities;
    using Forms;
    using Services;
    using ViewModels;

    public class HomeController : Controller
    {
        private const int PageSize = 10;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IPagingService _pagingService;

        public HomeController(IAuthenticationService authenticationService, IUnitOfWorkFactory unitOfWorkFactory, IPagingService pagingService)
        {
            _authenticationService = authenticationService;
            _unitOfWorkFactory = unitOfWorkFactory;
            _pagingService = pagingService;
        }

        public ActionResult Index(int page = 1)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.UserRepository.GetById(_authenticationService.GetCurrentUserId());

                var totalCount = user.Tasks.Count;

                var taskViewModels = user.Tasks
                    .Skip((page-1)*PageSize)
                    .Take(PageSize)
                    .ToArray()
                    .Select(task => new TaskViewModel
                    {
                        Id = task.Id,
                        Description = task.Description,
                        ExecutionDateTime = task.ExecutionDateTime.ToString("dd.MM.yyyy HH:mm"),
                        State = task.State,
                        IsDone = task.IsDone
                    })
                    .ToArray();

                var donePercent = GetTaskPercent(TaskState.Done, user.Tasks, totalCount);
                var todayScheduledPercent = GetTaskPercent(TaskState.TodayScheduled, user.Tasks, totalCount);
                var futureScheduledPercent = GetTaskPercent(TaskState.FutureScheduled, user.Tasks, totalCount);

                return View(new TaskBoardViewModel
                {
                    Tasks = taskViewModels,
                    Pagings = _pagingService.CreatePagings(PageSize, totalCount, page),
                    TaskStatesInfo = new TaskStatesInfoViewModel
                    {
                        DonePercent = donePercent,
                        TodayScheduledPercent = todayScheduledPercent,
                        FutureScheduledPercent = futureScheduledPercent,
                        OverduePercent = 100 - donePercent - todayScheduledPercent - futureScheduledPercent
                    }
                });
            }
        }

        private int GetTaskPercent(TaskState state, List<Task> tasks, int totalCount)
        {
            var stateCount = tasks.Count(x => x.State == state);

            return stateCount*100/totalCount;
        }

        [HttpPost]
        public ActionResult CreateTask(CreateTaskForm form)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.UserRepository.GetById(_authenticationService.GetCurrentUserId());

                var task = new Task(form.Description, form.ExecutionDateTime, user, form.IsDone);

                unitOfWork.TaskRepository.Insert(task);

                unitOfWork.Commit();
                return new EmptyResult();
            }
        }

        [HttpGet]
        public ActionResult EditTask(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = unitOfWork.TaskRepository.GetById(id);

                var form = new EditTaskForm
                {
                    Id = id,
                    Description = task.Description,
                    ExecutionDateTime = task.ExecutionDateTime,
                    IsDone = task.IsDone
                };

                return Json(form, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditTask(EditTaskForm form)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = unitOfWork.TaskRepository.GetById(form.Id);

                task.ExecutionDateTime = form.ExecutionDateTime;
                task.IsDone = form.IsDone;
                task.Description = form.Description;

                unitOfWork.TaskRepository.Update(task);

                unitOfWork.Commit();
                return new EmptyResult();
            }
        }

        [HttpPost]
        public ActionResult ChangeDoneState(ChangeDoneStateForm form)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = unitOfWork.TaskRepository.GetById(form.Id);

                task.IsDone = form.IsDone;

                unitOfWork.Commit();
                return new EmptyResult();
            }
        }
    }
}