namespace Web.Forms
{
    using System;

    public class CreateTaskForm
    {
        public bool IsDone { get; set; }
        public string Description { get; set; }
        public DateTime ExecutionDateTime { get; set; }
    }
}