namespace Web.Forms
{
    public class ChangeDoneStateForm
    {
        public int Id { get; set; }
        public bool IsDone { get; set; }
    }
}