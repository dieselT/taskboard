﻿namespace Web.Authentication
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using DataAccess;
    using NArms.Windsor;
    using Services;

    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        private readonly IUnitOfWorkFactory _uintOfWorkFactory;
        private readonly IAuthenticationService _authenticationService;

        public BasicAuthenticationAttribute()
        {
            _uintOfWorkFactory = IoC.Resolve<IUnitOfWorkFactory>();
            _authenticationService = IoC.Resolve<IAuthenticationService>();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_authenticationService.IsAuthenticated)
                return;

            var request = filterContext.HttpContext.Request;
            var authorizationHeaders = request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(authorizationHeaders))
            {
                var cred = Encoding.ASCII.GetString(Convert.FromBase64String(authorizationHeaders.Substring(6))).Split(':');
                var authorizationData = new { Login = cred[0], Password = cred[1] };
                using (var unitOfWork = _uintOfWorkFactory.Create())
                {
                    var user = unitOfWork.UserRepository.Get(x => x.Login == authorizationData.Login).FirstOrDefault();

                    if (user != null && user.Password.Check(authorizationData.Password))
                    {
                        _authenticationService.SignIn(user.Id);
                        return;
                    }
                        
                }
            }

            filterContext.HttpContext.Response.AddHeader("WWW-Authenticate", "Basic realm=\"Ryadel\"");
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}