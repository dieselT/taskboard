﻿namespace Web
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Authentication;
    using Castle.Windsor;
    using Castle.Windsor.Installer;
    using NArms.Windsor;
    using Windsor;

    public class MvcApplication : HttpApplication
    {
        private IWindsorContainer _container;

        private void BootstrapContainer()
        {
            _container = new WindsorContainer()
                .Install(FromAssembly.This());
            IoC.Init(_container);
            var controllerFactory = new WindsorControllerFactory(_container);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BootstrapContainer();
            GlobalFilters.Filters.Add(new BasicAuthenticationAttribute());
        }

        protected void Application_end()
        {
            _container.Dispose();
        }
    }
}