namespace Web.ViewModels
{
    using Domain.Definitions;

    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ExecutionDateTime { get; set; }
        public TaskState State { get; set; }
        public bool IsDone { get; set; }
    }
}