namespace Web.ViewModels
{
    using Controllers;
    using Models;

    public class TaskBoardViewModel
    {
        public TaskViewModel[] Tasks { get; set; }
        public TaskStatesInfoViewModel TaskStatesInfo { get; set; }
        public PagingElement[] Pagings { get; set; }
    }
}