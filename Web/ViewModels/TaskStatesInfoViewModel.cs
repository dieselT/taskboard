namespace Web.ViewModels
{
    public class TaskStatesInfoViewModel
    {
        public int DonePercent { get; set; }
        public int TodayScheduledPercent { get; set; }
        public int FutureScheduledPercent { get; set; }
        public int OverduePercent { get; set; }
    }
}