﻿$(function() {
    $(".dateTimePicker").datetimepicker({ format: "DD.MM.YYYY H:mm " });

    $("#addTask").on("click", function () {
        var url = $("#addTask").data("url");
        var data = {
            IsDone: $("#addTaskModal").find("#IsDone").is(":checked"),
            Description: $("#addTaskModal").find("#Description").val(),
            ExecutionDateTime: moment($("#addTaskModal").find("#executionDateTimePicker").data("DateTimePicker").date()).toISOString()
        };
        $.post(url, data)
            .done(function () {
                location.reload();
            });
    });

    $(".js-done-checkbox").on("click", function(event) {
        var url = $("#taskBoardContainer").data("change-done-state-url");
        var data = {
            Id: $(event.target).parent().siblings("#TaskId").val(),
            IsDone: $(event.target).is(":checked")
        };
        $.post(url, data)
            .done(function () {
                location.reload();
            });
    });

    $(".edit-task").on("click", function(event) {
        var url = $(event.target).data("url");
        var data = {
            Id: $(event.target).siblings("#TaskId").val()
        };
        $.get(url, data)
            .done(function (data) {
                $("#editTaskModal").find("#TaskId").val(data.Id);
                $("#editTaskModal").find("#IsDone").prop("checked", data.IsDone);
                $("#editTaskModal").find("#Description").val(data.Description);
                $("#editTaskModal").find("#executionDateTimePicker").data("DateTimePicker").date(moment(data.ExecutionDateTime));

                $("#editTaskModal").modal("show");
            });
    });

    $("#editTask").on("click", function () {
        var url = $("#editTask").data("url");
        var data = {
            Id: $("#editTaskModal").find("#TaskId").val(),
            IsDone: $("#editTaskModal").find("#IsDone").is(":checked"),
            Description: $("#editTaskModal").find("#Description").val(),
            ExecutionDateTime: moment($("#editTaskModal").find("#executionDateTimePicker").data("DateTimePicker").date()).toISOString()
        };
        $.post(url, data)
            .done(function () {
                location.reload();
            });
    });
});