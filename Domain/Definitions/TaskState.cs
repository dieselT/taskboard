﻿namespace Domain.Definitions
{
    public enum TaskState
    {
        Done,
        TodayScheduled,
        FutureScheduled,
        Overdue
    }
}