﻿namespace Domain
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using JetBrains.Annotations;

    public class Password
    {
        private static readonly Random Random = new Random();
        private static readonly HashAlgorithm HashAlgorithm = MD5.Create();

        [Obsolete("Only for ORM"), UsedImplicitly]
        protected Password()
        {
        }

        public Password(string password)
        {
            Salt = MakeSalt();
            Hash = HashPassword(password, Salt);
        }

        public virtual string Hash { get; protected internal set; }

        public virtual string Salt { get; protected internal set; }

        public virtual bool Check(string password)
        {
            return Hash == HashPassword(password, Salt);
        }

        private static string HashPassword(string password, string salt)
        {
            var encoding = Encoding.UTF8;
            var passwordBytes = encoding.GetBytes(password)
                .Union(Convert.FromBase64String(salt))
                .ToArray();
            return Convert.ToBase64String(HashAlgorithm.ComputeHash(passwordBytes).ToArray());
        }

        private static string MakeSalt()
        {
            return Convert.ToBase64String(Enumerable.Range(0, 5)
                .Select(_ => (byte)Random.Next()).ToArray());
        }
    }
}