﻿namespace Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class User : IEntity
    {
        [Obsolete("Only for ORM"), UsedImplicitly]
        public User(){}

        public User(string login, Password password)
        {
            if (string.IsNullOrEmpty(login))
                throw new ArgumentNullException(nameof(login), "User login can't be null or empty");

            if (password == null)
                throw new ArgumentNullException(nameof(password), "User password can't be null");

            Login = login;
            Password = password;
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public Password Password { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}