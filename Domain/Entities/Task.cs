﻿namespace Domain.Entities
{
    using System;
    using Definitions;
    using JetBrains.Annotations;

    public class Task : IEntity
    {
        [Obsolete("Only for ORM"), UsedImplicitly]
        public Task(){}

        public Task(string description, DateTime executionDateTime, User author, bool isDone = false)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentNullException(nameof(description), "Task description can't be null or empty");

            if (author== null)
                throw new ArgumentNullException(nameof(author), "Task author can't be null");

            Description = description;
            ExecutionDateTime = executionDateTime;
            IsDone = isDone;
            Author = author;
            AuthorId = author.Id;
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime ExecutionDateTime { get; set; }
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }
        public bool IsDone { get; set; }

        public TaskState State
        {
            get
            {
                if (IsDone)
                    return TaskState.Done;

                if (DateTime.Today.Date == ExecutionDateTime.Date)
                    return TaskState.TodayScheduled;

                if (DateTime.Today.Date < ExecutionDateTime.Date)
                    return TaskState.FutureScheduled;

                return TaskState.Overdue;
            }
        }
    }
}