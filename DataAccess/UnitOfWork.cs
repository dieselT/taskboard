namespace DataAccess
{
    using System;
    using Domain.Entities;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly TaskBoardContext _context;
        private bool _disposed;
        private RepositoryBase<User> _userRepository;
        private RepositoryBase<Task> _taskRepository;

        public UnitOfWork(TaskBoardContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public RepositoryBase<User> UserRepository => _userRepository ?? (_userRepository = new RepositoryBase<User>(_context));
        public RepositoryBase<Task> TaskRepository => _taskRepository ?? (_taskRepository = new RepositoryBase<Task>(_context));

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}