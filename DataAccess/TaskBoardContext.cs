﻿namespace DataAccess
{
    using System.Data.Entity;
    using Domain;
    using Domain.Entities;

    public class TaskBoardContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<Password>();

            modelBuilder.Entity<User>()
                .HasMany(x => x.Tasks)
                .WithRequired()
                .HasForeignKey(x => x.AuthorId);

            modelBuilder.Entity<Task>()
                .Ignore(x => x.State);

            base.OnModelCreating(modelBuilder);
        }
    }
}