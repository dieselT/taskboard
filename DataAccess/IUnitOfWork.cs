namespace DataAccess
{
    using System;
    using Domain.Entities;

    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        RepositoryBase<User> UserRepository { get; }
        RepositoryBase<Task> TaskRepository { get; }
    }
}