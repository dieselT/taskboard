namespace DataAccess.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Domain;
    using Domain.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<TaskBoardContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TaskBoardContext context)
        {
            const string user1Login = "test";
            const string user2Login = "test2";

            context.Users.AddOrUpdate(
                user => user.Login,
                new User(user1Login, new Password("test")),
                new User(user2Login, new Password("qwerty"))
                );

            context.SaveChanges();

            var user1 = context.Users.First(x => x.Login == user1Login);
            var user2 = context.Users.First(x => x.Login == user2Login);

            var tasks = new List<Task>();

            var user1TasksCounter = 0;
            var user2TasksCounter = 0;
            for (var i = 0; i < 1000; i++)
            {
                Task task;
                if (i % 2 != 0)
                {
                    task = new Task($"Task {i} description", CreateExecutionDate(user1TasksCounter), user1);
                    user1TasksCounter++;
                }
                else
                {
                    task = new Task($"Task {i} description", CreateExecutionDate(user2TasksCounter), user2);
                    user2TasksCounter++;
                }
                tasks.Add(task);
            }

            context.Tasks.AddOrUpdate(
                task => task.Description,
                tasks.ToArray()
                );
        }

        private DateTime CreateExecutionDate(int tasksCounter)
        {
            var executionDate = DateTime.Today.AddHours(15);

            if (tasksCounter%3 == 0)
                executionDate = executionDate.AddDays(tasksCounter);
            else if (tasksCounter%2 == 0)
                executionDate = executionDate.AddDays(-tasksCounter);
            return executionDate;
        }
    }
}
