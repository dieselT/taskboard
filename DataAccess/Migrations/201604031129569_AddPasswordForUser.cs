namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPasswordForUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Password_Hash", c => c.String());
            AddColumn("dbo.Users", "Password_Salt", c => c.String());
            DropColumn("dbo.Users", "Password");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Password", c => c.String());
            DropColumn("dbo.Users", "Password_Salt");
            DropColumn("dbo.Users", "Password_Hash");
        }
    }
}
